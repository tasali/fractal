[package]
name = "fractal"
version = "8.0.0"
authors = ["Julian Sparber <julian@sparber.net>"]
edition = "2021"
rust-version = "1.76"
publish = false

[profile.release]
debug = true
lto = "thin"
codegen-units = 1

[profile.dev.package."*"]
opt-level = 3
debug = false
debug-assertions = false
overflow-checks = false
incremental = false
codegen-units = 16

# Please keep dependencies sorted.
[dependencies]
djb_hash = "0.1"
futures-channel = "0.3"
futures-util = "0.3"
geo-uri = "0.2"
gettext-rs = { version = "0.7", features = ["gettext-system"] }
image = "0.25"
indexmap = "2"
linkify = "0.10.0"
mime = "0.3"
mime_guess = "2"
once_cell = "1"
pulldown-cmark = "0.11"
qrcode = "0.14"
rand = "0.8"
regex = "1"
rmp-serde = "1"
rqrr = "0.7"
secular = { version = "1", features = ["bmp", "normalization"] }
serde = "1"
serde_json = "1"
strum = { version = "0.26", features = ["derive"] }
thiserror = "1"
tld = "2"
tokio = { version = "1", features = ["rt", "rt-multi-thread", "sync"] }
tokio-stream = { version = "0.1", features = ["sync"] }
tracing = "0.1"
tracing-subscriber = { version = "0.3", features = ["env-filter"] }
url = "2"

# gtk-rs project and dependents. These usually need to be updated together.
adw = { package = "libadwaita", version = "0.7", features = ["v1_5"] }
gst = { version = "0.23", package = "gstreamer" }
gst_base = { version = "0.23", package = "gstreamer-base" }
gst_gtk = { version = "0.13", package = "gst-plugin-gtk4" }
gst_pbutils = { version = "0.23", package = "gstreamer-pbutils" }
gst_play = { version = "0.23", package = "gstreamer-play" }
gst_video = { version = "0.23", package = "gstreamer-video" }
gtk = { package = "gtk4", version = "0.9", features = ["gnome_45"] }
shumate = { package = "libshumate", version = "0.6" }
sourceview = { package = "sourceview5", version = "0.9" }

[dependencies.matrix-sdk]
git = "https://github.com/matrix-org/matrix-rust-sdk.git"
rev = "92b4c2a469f507696fa9db3d6bdb000a761e2694"
features = [
    "socks",
    "sso-login",
    "markdown",
    "qrcode",
    "image-rayon",
]

[dependencies.matrix-sdk-ui]
git = "https://github.com/matrix-org/matrix-rust-sdk.git"
rev = "92b4c2a469f507696fa9db3d6bdb000a761e2694"
default-features = false
features = ["e2e-encryption", "native-tls"]

[dependencies.ruma]
# version = "0.10"
git = "https://github.com/matrix-org/ruma.git"
rev = "4d3d8b46fd519012e4585ccf00dbea1eb602c028"
features = [
    "unstable-unspecified",
    "client-api-c",
    "compat-key-id",
    "compat-user-id",
    "compat-empty-string-null",
    "compat-null",
    "compat-optional",
    "compat-unset-avatar",
    "compat-get-3pids",
    "html-matrix",
]

# Linux-only dependencies.
[target.'cfg(target_os = "linux")'.dependencies]
ashpd = { version = "0.9", default-features = false, features = [
    "pipewire",
    "tracing",
    "tokio",
] }
oo7 = { version = "0.3", default-features = false, features = [
    "openssl_crypto",
    "tokio",
    "tracing",
] }
