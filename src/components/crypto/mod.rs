//! Widgets for setup and management of the crypto identity and recovery.

mod identity_setup_view;
mod recovery_setup_view;

pub use self::{identity_setup_view::*, recovery_setup_view::*};
